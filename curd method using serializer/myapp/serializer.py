from rest_framework import serializers
from .models import Person 

class PeopleSerializer(serializers.ModelSerializer):
    class meta:
        model=Person
        fields='__all__'
        

